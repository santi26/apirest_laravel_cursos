<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; // esta libreria se debe importar  para poder utiizar el validador
use Illuminate\Support\Facades\Hash; // esto se debe importar para utilizar la encriptacion de los datos o hash
use App\Clientes; // instanciamos el modelo para poder usarlo




class ClientesController extends Controller
{
    //

    Public function index(){
        $json = array(
            'detalle'=> 'No encontrado'


        );
        echo json_encode($json , true);



    }


    public function store(Request $request){ //por $request recibo los valores del formulario

        //recoger datos
        
        $datos = array(
            "primer_nombre" =>$request->input("primer_nombre"),
            "primer_apellido" =>$request->input("primer_apellido"), //capturamos los datos que podrian venir de un formulario con ese nombre que esta en el input
            "email" =>$request->input("email")
         );

         if(!empty($datos)){
             $validator = Validator::make($datos, [
            'primer_nombre' => 'required|string|max:255',
            'primer_apellido' =>'required|string|max:255',
            'email'=>'required|string|email|max:255|unique:clientes'
            ]);


            if ($validator->fails()) {

                $json = array(
                'status'=>404,// no existe o no lo encuentra en el servidor    
                'detalle'=>'registro con errores'
                );
                return json_encode($json, true);
            }else{ // aprobamos la validacion 
            
                // generamos el id cliente y la llave secreta para devolver al cliente
                $id_cliente =  Hash::make($datos["primer_nombre"].$datos["primer_apellido"].$datos["email"]);//concatenamos cada posicion del array y luego generamos la clave alfanumerica       
                //echo '<pre>'; print_r($id_cliente); echo '<pre>';
                $llave_secreta =  Hash::make($datos["email"].$datos["primer_apellido"].$datos["primer_nombre"], ["rounds"=>12]);

                //echo '<pre>'; print_r($id_cliente); echo '<pre>'; sacamos pantalla el contenido de la variable
                $id_cli = str_replace('$' , 'a' , $id_cliente); //reemplazamos todo lo que venga con pesos con la a al generar el token porque con el signo $ no son iguales en la autentificacion
                $llave_sec = str_replace('$' , 'o' , $llave_secreta);
            
                //guardamos datos del cliente
                $cliente = new Clientes();//generamos una instancia del modelo de cliente
                $cliente->primer_nombre = $datos["primer_nombre"];
                $cliente->primer_apellido = $datos["primer_apellido"];
                $cliente->email = $datos["email"];
                $cliente->id_cliente = $id_cli; 
                $cliente->llave_secreta = $llave_sec;

                $cliente->save(); //guardamos los datos en la tabla clientes de la db

                $json = array(
                    'status'=>200, //ok
                    'detalles'=>'registro exxitoso, tome sus credenciales y guardelas',
                    'credenciales'=>array(
                    'id_cliente'=>$id_cli,
                    'llave_secreta'=>$llave_sec
                    )
                );
                return json_encode($json, true);




            }

        }else{//si el array de datos viene vacio

            $json = array(
                'status'=>404,    
                'detalle'=>'registro con errores'
                );
                return json_encode($json, true);

        }    


     }
}


