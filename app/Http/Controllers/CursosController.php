<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cursos; // instanciamos el modelo para poder usarlo
use App\Clientes; //instanciamos el modelo de clientes
use Illuminate\Support\Facades\Validator;

class CursosController extends Controller
{
    //

    public function index(Request $request){
        //mostrar todos los registros

        $token = $request->header('Authorization'); //traemos el token que se genero en postman en el encabezado de  authorization

         //echo '<pre>'; print_r($token); echo '<pre>';

         $clientes = Clientes::all();

         $json = array();

         foreach ($clientes as $key => $value){
             if("Basic ".base64_encode($value["id_cliente"].":".$value["llave_secreta"]) == $token) {//si la combinacion de base64 del valor del idcliente de la db + la llave_secreta es igual al token que recibimos de postman por la reuqest

                $cursos = Cursos::all(); //traemos todos los datos de los cursos instanciando este modelo y con el metodo all

                if(!empty($cursos)){ // si no viene vacio el array de cursos
                    $json = array( //generamos la respuesta que vamos a dar al usuario
                    'status'=>200,    
                    'total_registros'=>count($cursos),//muestra el total de los cursos por ej 60
                    'detalle'=>$cursos
                    );
                    //return json_encode($json, true);
                }else{// si el array de cursos viene vacio
                    
                    $json = array( //generamos la respuesta que vamos a dar al usuario
                        'status'=>200,    
                        'total_registros'=>0,//muestra el total de los cursos por ej 60
                        'detalle'=>'no hay ningun curso registrado'
                        );
                        //return json_encode($json, true);
        
        
                }





             }else{ // si no ocurre la autorizacion

                $json = array( //generamos la respuesta que vamos a dar al usuario
                    'status'=>404,    
                    'total_registros'=>0,//muestra el total de los cursos por ej 60
                    'detalle'=>'no esta autorizado para recibir los registros'
                    );
                    //return json_encode($json, true);


             }


         }// fin foreach

        

         return json_encode($json, true);


    }
    //crear un nuevo registro

    public function store(Request $request){
        
        $token = $request->header('Authorization'); //traemos el token que se genero en postman en el encabezado de  authorization

         //echo '<pre>'; print_r($token); echo '<pre>';

         $clientes = Clientes::all();

         $json = array();
        
         foreach ($clientes as $key => $value){
            if("Basic ".base64_encode($value["id_cliente"].":".$value["llave_secreta"]) == $token) {


                //recoger datos
                $datos = array(
                    "titulo" =>$request->input("titulo"),
                    "descripcion"=>$request->input("descripcion"), //capturamos los datos que podrian venir de un formulario con ese nombre que esta en el input
                    "instructor"=>$request->input("instructor"),
                    "imagen"=>$request->input("imagen"),
                    "precio"=>$request->input("precio"),

                 );

                 //echo '<pre>'; print_r($datos); echo '<pre>';

                 //return;

                 //validar datos
                 if(!empty($datos)){
                    $validator = Validator::make($datos, [
                   'titulo' => 'required|string|max:255|unique:cursos',
                   'descripcion' =>'required|string|max:255|unique:cursos',
                   'instructor'=>'required|string|max:255',
                   'imagen'=>'required|string|max:255|unique:cursos',
                   'precio'=>'required|numeric'
                   
                   ]);
                   
                   //si falla la validacion
                   if ($validator->fails()) {

                        $json = array(
                        'status'=>404,// no existe o no lo encuentra en el servidor    
                        'detalle'=>'registro con errores: posible titulo, descripcion o imagen repetidos, no se permiten caracteres especiales'
                        );
                        return json_encode($json, true);// con return cancelamos todo lo que venga por debajo  y retornamos solo el valor de esa condicion
                        //si queremos validar de a un campo tomamos validator 1 o 2 o3 vamos colocando con las condiciones de a uno y devolviendo un json para cada caso
                    }else{

                        $cursos = new Cursos();//instanciamos la clase del modelo cursos


                        $cursos->titulo = $datos['titulo'];
                        $cursos->descripcion = $datos['descripcion'];
                        $cursos->instructor = $datos['instructor'];
                        $cursos->imagen = $datos['imagen'];
                        $cursos->precio = $datos['precio'];
                        $cursos->id_creador = $value['id']; // este dato traemos del cliente del id que esta en el foreach que rrecorremos cuando el cliente cumple con la condicion

                        $cursos->save();

                        $json = array(
                            'status'=>200,// no existe o no lo encuentra en el servidor    
                            'detalle'=>'su curso fue creado exitosamente'
                            );
                            return json_encode($json, true);                        

                    }
                    

                 

             
               }else{ //si el array de datos esta vacio

                    $json = array(
                    'status'=>404,    
                    'detalle'=>'los registros no pueden estar vacios'
                    );
                    return json_encode($json, true);
                

               }
            }
                
          }
          return json_encode($json, true);

    }


     //como hariamos si x persona tiene una pag y quisiera traer toda la informacion para no tener que volver a escribir desde cero?
     // traer un solo registro con el metodo show

     public function show($id, Request $request){

        $token = $request->header('Authorization'); //traemos el token que se genero en postman en el encabezado de  authorization

        //echo '<pre>'; print_r($token); echo '<pre>';

        $clientes = Clientes::all();

        $json = array();
       
        foreach ($clientes as $key => $value){
           if("Basic ".base64_encode($value["id_cliente"].":".$value["llave_secreta"]) == $token) {

                $curso = Cursos::where("id" , $id)->get(); //obtenemos el curso que coincide en el id con el que pasamos por el endpoint

                if(count($curso) > 0){ // si no viene vacio el array de curso
                    $json = array( //generamos la respuesta que vamos a dar al usuario
                    'status'=>200,    
                    'detalle'=>$curso //devolvemos un solo curso 
                    );
                    //return json_encode($json, true);
                }else{// si el array de cursos viene vacio
                
                    $json = array( //generamos la respuesta que vamos a dar al usuario
                    'status'=>404,    
                    'detalle'=>'no hay ningun curso registrado con ese id'
                    );
                    //return json_encode($json, true);
    
    
                }

               
                                                  

            }else{ // si no ocurre la autorizacion

                $json = array( //generamos la respuesta que vamos a dar al usuario
                    'status'=>404,    
                    'detalle'=>'no esta autorizado para recibir el registro'
                    );
                    //return json_encode($json, true);


            }
                   

        } //fin foreach
        return json_encode($json, true);   
    }






     






    public function update($id, Request $request){
        
        $token = $request->header('Authorization'); //traemos el token que se genero en postman en el encabezado de  authorization

         //echo '<pre>'; print_r($token); echo '<pre>';

         $clientes = Clientes::all();

         $json = array();
        
         foreach ($clientes as $key => $value){
            if("Basic ".base64_encode($value["id_cliente"].":".$value["llave_secreta"]) == $token) {//hacemos la autorizacion


                //recoger datos
                $datos = array(
                    "titulo" =>$request->input("titulo"),
                    "descripcion"=>$request->input("descripcion"), //capturamos los datos que podrian venir de un formulario con ese nombre que esta en el input
                    "instructor"=>$request->input("instructor"),
                    "imagen"=>$request->input("imagen"),
                    "precio"=>$request->input("precio"),

                 );

                 //echo '<pre>'; print_r($datos); echo '<pre>';

                 //return;

                 //validar datos
                 if(!empty($datos)){
                    $validator = Validator::make($datos, [
                   'titulo' => 'required|string|max:255',
                   'descripcion' =>'required|string|max:255',
                   'instructor'=>'required|string|max:255',
                   'imagen'=>'required|string|max:255', // el unique no va porque si quiere modificar no dejaria guardar si vuelve a traer el mismo dato desde postman nos va a decir q la imagen el titulo y descripcion debe ser unica y eso no queremos
                   'precio'=>'required|numeric'
                   
                   ]);
                   
                   //si falla la validacion
                   if ($validator->fails()) {

                        $json = array(
                        'status'=>404,// no existe o no lo encuentra en el servidor    
                        'detalle'=>'registro con errores: no se permiten caracteres especiales'
                        );
                        return json_encode($json, true);// con return cancelamos todo lo que venga por debajo  y retornamos solo el valor de esa condicion
                        //si queremos validar de a un campo tomamos validator 1 o 2 o3 vamos colocando con las condiciones de a uno y devolviendo un json para cada caso
                    }else{

                        //validamos que el que este editando el registro sea el mismo creador del registro
                        //pedimos con el id que me traiga toda la informacion de ese curso
                        $traer_curso = Cursos::where("id", $id)->get(); //me va a traer un array de un solo indice para este caso

                        if ($value['id'] == $traer_curso[0]['id_creador']){ //si el id del cliente = al id creador que viene del array de la db de cursos
                            // aca creamos un nuevo array $datos con la informacion del array de datos uqe vino por la request
                            $datos = array("titulo"=>$datos["titulo"],
                                            "descripcion"=>$datos["descripcion"],
                                            "instructor"=>$datos["instructor"],
                                            "imagen"=>$datos["imagen"],
                                            "precio"=>$datos["precio"]


                                          
                        
                            );

                            $cursos = Cursos::where("id", $id)->update($datos); //trae el curso que tenga una coincidencia con el id que pasamos como param en la request
                           
                            $json = array(
                                'status'=>200,    
                                'detalle'=>'su curso fue modificado exitosamente, sus datos han sido guardados'
                                );
                                return json_encode($json, true);   

                        }else{

                            $json = array(
                                'status'=>404,    
                                'detalle'=>'no esta autorizado para modificar este curso'
                                );
                                return json_encode($json, true);  

                        } 




                                            

                    }
                    

                 

             
               }else{ //si el array de datos esta vacio

                    $json = array(
                    'status'=>404,    
                    'detalle'=>'los registros no pueden estar vacios'
                    );
                    return json_encode($json, true);
                

               }
            }
                
          } //fin foreach
          return json_encode($json, true);

    }


    public function destroy($id , Request $request){

        $token = $request->header('Authorization'); //traemos el token que se genero en postman en el encabezado de  authorization

        //echo '<pre>'; print_r($token); echo '<pre>';

        $clientes = Clientes::all();

        $json = array();
       
        foreach ($clientes as $key => $value){
           if("Basic ".base64_encode($value["id_cliente"].":".$value["llave_secreta"]) == $token) {

                $curso = Cursos::where("id" , $id)->get();

                if(!empty($curso)){ // si no viene vacio el array de curso es porque el curso si existe
                    //si el curso existe comprobamos que el que lo este borrando sea el creador del curso
                    if($value['id'] == $curso[0]["id_creador"]){ // si se cumple esto procedemos a borrar el curso

                        $cursoAborrar = Cursos::where("id" , $id)->delete();

                        $json = array( //generamos la respuesta que vamos a dar al usuario
                            'status'=>200,    
                            'detalle'=>'se ha borrado el curso exitosamente: '.' '.$curso //devolvemos el curso eliminado
                            );

                    }else{

                        $json = array( //generamos la respuesta que vamos a dar al usuario
                            'status'=>404,    
                            'detalle'=>'no esta autorizado a eliminar este curso por no ser el creador'
                            );


                    }
                
                //return json_encode($json, true);
                }else{// si el array de cursos viene vacio
                
                    $json = array( //generamos la respuesta que vamos a dar al usuario
                    'status'=>404,    
                    'detalle'=>'el curso que se desea eliminar no existe'
                    );
                    //return json_encode($json, true);
    
    
                }

               
                                                  

            }else{ // si no ocurre la autorizacion

                $json = array( //generamos la respuesta que vamos a dar al usuario
                    'status'=>404,    
                    'detalle'=>'no esta autorizado para eliminar el curso'
                    );
                    //return json_encode($json, true);


             }
                   

        } //fin foreach
          return json_encode($json, true);


    } //fin destroy   



} //fin clase cursocontroller
//hemos generado la autorizacion de token por cabecera para que las personas obligatoriamente se tengan que registrar y generar el token para poder tomar los datos