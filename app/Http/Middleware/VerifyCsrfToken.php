<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //

        'http://apirest-laravel.com/registro',
        'http://apirest-laravel.com/cursos', //hacemos la excepcion que no verifique csrf token
        'http://apirest-laravel.com/cursos/*'//pasamos una ruta con un parametro en este caso sera el id del curso que se va a editar
    ];
}
